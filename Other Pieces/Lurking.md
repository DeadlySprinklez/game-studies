# Lurking, and Why it is the Most Beautiful, Powerful, Dangerous Thing On The Internet

Lurking, a verb, is hiding away from another being or group of beings and
trying not to be noticed. It is the epitome of stealth. Back in the early
caveman days, it was used for hunting. When applied to modern day
civilization, when hunting is done in reserves and forests, when people are
living their lives in cities instead of wilds, can lurking really be used?

Yes, actually. And it is the most overpowered move on the internet.

### On the Internet, No One Can Tell You're a Dog

The internet is a strange place. Actually, it's not even a *place*. It's
commonly referred to as a series of tubes, an invisible spider web, but in
reality the internet is just a series of servers hosting websites connected
to services which are connected to other services...it's complicated. The
internet is complicated. And when you're on the internet, anonymity is an
ever-present being. When you're on the internet, you're often found hiding
under the guise of a screen name. You're connecting to millions upon
millions of people at once, some of them want to steal your data, your
money, your personality, and you're gonna kick that off by voluntarily
giving anyone you see your full name? No way. So many people start to form
these alternate personalities, these names. Mine is DeadlySprinklez, and
there's a story behind that one. There are stories behind many of them.
Many different quirks, several different contexts, formed over years and
years of experience over the web. You begin to form friends under this
name, find communities, forums, instant messaging clients, video games, all
different types of different places and different people. Sometimes they're
halfway across the world, and sometimes, well, if you get lucky, you make a
friend next door right from your bedroom.

But when the night rolls over, and the services you use go quiet, everyone
expects you to be in bed, snoozing and waiting for the day to roll in for
you to wake up and for everybody to chat again. For the debates to start,
the laughs to be had. Everyone waits, expectantly, when you go out during
the day to a store, to school. What if, when you wait those ten minutes and
the little status bubble next to your name goes from green to yellow,
yellow to blank, you're not actually away from your keyboard? Secretly,
you're reading these messages on your phone. You're seeing everyone's
reactions over the net while they think you're gone.

You are invisible. You have gained this power through manipulation of power
and guise, and you have successfully fooled at least some of your friends
into thinking you had actually gone to do whatever it is you wanted to do.
But in reality, you're right there. An omnipresent god. The only thing you
aren't able to do is join voice calls, video calls, participate in chat.
You're looking out from the inside. You can't interact. You, you cheeky
devil, have essentially caged yourself into torture. You get to see what
antics go on at night when you're dreaming away, but you don't get to
participate. Maybe you find things that you aren't ready for, like how your
friends talk behind your back about you or what they think of X and Y while
you're away, or like how your friend group is surprisingly devoid of any
conversation when you're not there.

Lurking is the most inherently powerful thing you can use on the internet.
The information you learn can help you going forward. But it's also
volatile, even at times when you don't think it would be. Pieces of info
that you wouldn't be able to bring up otherwise are added to your arsenal
of knowledge, able to be used against your friends, if you so choose, in a
bit of debate that may or may not be brought up later on. New situations
that show new sides of your friends, you get to learn more about them,
potentially even the parts that you don't want to know about them. It's a
stealth operation with the easiest objective possible: watch and listen.
You're already in the perfect spot to eavesdrop.

But, of course, this entire article is insane and why would you do that
when you could be working, sleeping, doing something better with your time
than just looking online when mostly everyone is asleep or also doing work
like *you should be WHY ARE YOU LIKE THIS OH MY GOD JUST DO WHAT YOU NEED TO
DO.*

Lurking *can* be the most powerful, beautiful, dangerous tool on the
internet. But when you're in normal communities, with normal friends,
lurking is just a fun game of hide and seek with the friends who know you
aren't actually asleep. It's a time to participate and interact with the
people on the other ends of the world who are asleep whenever you're awake.
It's a chance to see the night life of communities and what goes on when
you're not present.