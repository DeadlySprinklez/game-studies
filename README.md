# Game Studies

A series of analytical essays that go in-depth as to what makes a game the way it is, as well as the effort that goes into it.