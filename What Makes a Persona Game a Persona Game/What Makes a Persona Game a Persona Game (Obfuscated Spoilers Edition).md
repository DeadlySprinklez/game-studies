# What Makes a Persona Game a Persona Game

	This version is no longer being upheld. The main article is more updated
	than this one currently is. If you would like to read more, please finish
	the Persona games listed in the Spoiler Warning section below and, when you
	do, click the link below.

[Go to the main article!](https://bitbucket.org/DeadlySprinklez/game-studies/src/master/What%20Makes%20a%20Persona%20Game%20a%20Persona%20Game.md)

Atlus' Persona series has been long standing, and the latest entry, Persona
5, is now getting it's own expansion and upgraded Switch release. Persona 5
Royal is launching in Japan later this year, and seeing a trailer for it
made me wish I had a Switch or a PS4 even more than I already do now.
However, contemplating on this fact, I allowed my mind to wander a bit and
I came to a conclusion that many have came to already: Persona is a good
series. That's not the only conclusion I came to, however. It's a good
series because of it's traits. Let's dive a bit deeper here.

## Spoiler Warning

Just as a forewarning, this study goes in depth with the games Persona 3
FES, Persona 4 Golden, and Persona 5. If you have yet to experience these
games for yourself, please distance yourself from this study until you
have. Persona is a good game series, and I can vouch for them personally,
if that means anything to you at all.

### The Series' Patterns

**Disclaimer:**
I'm going to be honest - I can't stomach watching past Persona 3 FES. To
clarify, I've watched through Persona 5, Persona 4 Golden, and Persona 3
The Journey and The Answer in their entirety, but I can't seem to be hooked
past that. Persona 1 is outdated, and the visuals...well, they haven't aged
well, let's say. The cutscenes were definitely ahead of it's time, though.
Persona 2 does slightly better on this, but as far as I can tell, neither
game has close to voice acting outside of cutscenes. Most of my conclusions
are drawn from Persona 3 onwards. Please keep that in mind.

Persona, as a game series, has many patterns. The games all have things in
common. Other worlds, modernity, Persona, awakening scenes, Igor, but most
importantly in my eyes, they give the character and, through them, the
player motivation to explore this other world. In Persona 3, the main
character is an orphan who recently transferred to a new school and was
"accidentally" placed in the wrong dorm, with the promise that they would
be moved soon. Later on, we discover that it's not only the main character
who lost their family, but it's actually the entire cast who had been left
with at least one missing family member due to an explosion that had
happened years prior. This explosion was what had created the Dark Hour in
the game, which happens at exactly midnight and spawns Tartarus, the
presumably endless tower that only appears in your school courtyard.

In Persona 4, your character has no initial motive and is just a transfer
student who had started living with their uncle, who just so happened to be
a cop, and his daughter. As time goes on, you learn of the Midnight
Channel, and you find that it's directly related to a series of deaths that
had been occuring around your town. They appear to be homicides, but no one
is able to find the killer. That ends up turning into the player's quest
when they hear a voice telling them to enter the television, and, soon
after, they discover that one of their friends had ended up on the
aforementioned channel.

In Persona 5, the motive is near immediately revealed by the introduction.
The player character is shown running and jumping through a casino's upper
level, along chandeliers and railings with a briefcase in hand. Shortly
after pulling a slick move to escape the casino through some stained glass,
the police capture the MC and the first half of the game is taken place
inside of an interrogation. During the interrogation, the main character
frequently refers back to one moment that happened before they begun work
as a Phantom Thief, where he attempts to save a woman from being harassed
and abused by a drunk politician. The MC pushes the drunk man away from the
woman, and the drunk man ends up threatening to sue the main character,
who, at the time, was only going through their first year of high school.

All of these motives are injustices pushed upon the main character, and by
relation, the player. Persona is a game series revolving around these
injustices, and around the player assisting the MC in solving the
injustices and righting the wrongs.

Persona is a series of JRPGs, as well. No JRPG is complete without multiple
teammates, and saying Atlus' Persona series is no different is a lie.
Persona takes the traditional multiple teammates system to another level.
Social links are a common trope in Persona. The bonds you form with your
teammates not only makes them stronger, not only makes you stronger, but it
also unlocks special abilities. In Persona 5, a common ability for one to
gain is the Baton Pass, which passes on one character's turn to power up
another character. A common ability in Persona as a whole is one where
characters will stand in front of the main character's way in battle if an
attack would prove lethal for them. It's useful because if a teammate goes
down, they can be revived, but if the main character falls in battle, it's
an instant game over. It's a brutal mentality to have, thinking of it as
just a useful skill, because the ability actually proves the bond between
your character and the friends they made along the way.

Social links aren't only limited to the party members, however. You can
form bonds with people outside of the know of the various worlds the MC
travel, whether it's the Metaverse, the TV World, or Tartarus.

#Spoilers in this paragraph!
For example, Persona 5 has a teacher who works as a maid on the side to pay
off the parents of her former student who commited suicide, a student who
you inadvertedly saved from abuse and afterwards helps you from the
sidelines, a fortune teller who was wronged long ago, a non-corrupt
politician who wants to repent for his past sins, a famous shogi player who
wants to escape her fame, a shooter-genre addicted young arcade gamer with
an abusive mom, a journalist who gets drunk nearly every night, a model gun
salesman with past ties to a mafia but now has a family, a doctor who wants
to create a medicine for a patient she failed to cure but struggles after
she was removed from a big medical company, and the owner of the cafe you
live at to befriend and grow bonds with, none of which know about how you
go around stealing distorted desires in the form of Treasures. Each of the
different bonds give their own different and unique abilities both before
and after you save them from their issues (which usually all involve saving
another person from themselves and their own distorted desires) and also
allows you to connect deeper with the characters in the world around you.
External social links are present in every Persona game, and they all help
out in battle and out of battle.
##Spoilers over!

To summarize, Persona is a series about righting wrongs and growing
relationships. Each main character is given their motivation, and the
player connects with them through it. The player wants to believe that if
their motivation came, and that they were given the chance to change lives
and save people from the issues that the player had experienced beforehand,
that they would spring to action like the Phantom Thieves in Persona 5 or
the rag-tag group of school friends in Persona 4 or SEES in Persona 3 had
done.

...but what does the main character have that every other character does
not?

### The "Chosen One" Trope

At the start of every Persona game, the main character is dubbed the
"wildcard", usually associated with the Fool arcana. The wildcard, the
trickster, the fool, the main character is the one character who is known
for being able to capture, combine, and use different Personas, which are
referred to as different "masks" or personalities, essentially meaning that
each Persona is a different personality within the character. The wildcard
ability makes every different Persona resonate with the main character,
allowing them to use their power. Igor goes further into this in-game, when
the player discovers their power. The wildcard is also usually the mark of
change, where the main character is the only one able to change fate and
avoid death and ruin. If they fall in battle, miss the deadline, or fail
otherwise along the way, their future becomes hazed and unable to be
changed anymore, resulting in a game over. This is especially shown in
Persona 5, where if you fail in any way, your character *literally dies.* As
in, if the player misses a deadline, they are arrested and they get shot
in the head by a mystery character, who is revealed later in the story.

The player is often seen as a sort of chosen one *because* of the injustice
served upon them. They are given this ability because of the incidents
happening before them, a higher power sees what happens and deems them
worthy of changing their fate as the future continues. An odd change
between Persona 4 and Persona 5 is that in both Persona 3 and 4, the Velvet
Rooms, which is where Igor resides with his assistants and where he meets
with the player, were both portrayed as forms of transport, pushing you
towards your future. In Persona 3, it was an elevator, pushing the
character upwards until they reach their floor. In Persona 4, the Velvet
Room was a limousine, constantly moving towards a certain point until the
end of the game where the car stops and the destination is reached. Both of
these are heavily implied that you are moving towards your future. However,
in Persona 5, the Velvet Room is a prison instead. It is directly stated in
Persona 5 by Igor that the Velvet Room is the wildcard's team's own
manifestation of being imprisoned by the rules of the world. The prison,
however, is used for rehabilitation. This can be seen as the end goal for
the Velvet Room, in a sense, whereas the elevator and limousine reached the
future, the rehabilitation of the main character is them growing as a
person and being able to change their fate through that.

### Personas Are Just Edgy Pokémon

Persona is a **JRPG** series, or a **J**apanese **R**ole **P**laying **G**ame. This genre of
game usually consists of a level system, where the characters gain
experience points to increase an arbitrary rank. This usually grows the
character's strength and other latent abilities.