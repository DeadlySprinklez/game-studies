# Minecraft's Absurd Popularity

The voxel-based first person builder and brawler game *Minecraft* developed
by Markus "Notch" Persson and now handled by *Mojang* and *Microsoft* was a one
man project started in 2009 as a Java project. Notch sold *Mojang Studios*,
and, as a result, *Minecraft*, to *Microsoft* for multi-millions. But why was
*Minecraft* so well received? What had happened to make it gain this massive
of a popularity spike? For a game that's so simplistic at first glance,
many people would never have given it a second thought. Was it luck? Was it
fate? Did Notch somehow pay off some internet black market to buy his game
for him to artificially boost sales?

Well, no. That'd be criminally stupid. But it was somewhat luck.

### The Block Placed 'Round The World

*Minecraft* was started in Java, and is still maintained in Java. This does
not seem to be changing any time soon, although it is being upheld in C as
well in the form of *Minecraft: Bedrock Edition*, which is now just
referred to as the definitive version of *Minecraft.* Despite this, many
people still refer to the newly dubbed *Minecraft: Java Edition* as their
preferred version and the true version. The Java Edition is still upheld,
and still gets regular updates. The 1.14 version was just recently pushed
for release, as a matter of fact, at the time of this article's creation.

Java was a good choice for creating *Minecraft*. As an Object Oriented
Programming language, Notch had access to classes which made modifying game
mechanics much easier in the form of mods. This, combined with extensive
knowledge of how Java works and how mods could change the game for the
better, led to the creation of many loaders and "patchers" for modification
of the blocky game. Initial mods were limited, required no patching,
and consisted mostly of flymods and noclip.

The creations of mods led to a term for unmodded *Minecraft*. Any client or
game version that was left unedited, essentially just the version retrieved
from *Mojang*'s servers themselves, was dubbed "vanilla." In the early
days, Vanilla was the only version that was, in a sense, entry-level to
every user. Modification before about Beta 1.9 was difficult, yet some folk
still managed to get through it. User Risugami had uploaded their own
patcher, ModLoader, in the early Beta days, which was the most popular
choice back then. This is what most people used back then, including the
famous YouTube channel *BlueXephos*, more well known nowadays as *The Yogscast*
members Lewis and Simon.

### I AM DAVE YOGNAUGHT

*The Yogscast* had played a huge part in the growth in popularity for
*Minecraft*. They originally started making videos on *Minecraft* with their
Survival Island series and eventually branched further out, doing community
made levels known as Adventure Maps, which usually consisted of puzzles and
exploration in a custom world. They eventually started doing a story series
with acting performed by themselves and other members of *The Yogscast*
called *Shadow of Israphel*, named after the series' main villain who was a
player in a dark suit and with a white head branded with a creeper's face.
The series took a turn for the worst eventually as it came to a halt midway
through, but the people who watched the series were absolutely captivated.

Eventually the duo teamed up with other people in their team to do more
series. A popular one was their *Technic Pack* Tekkit series, where their
goal was to make infinite Jaffa Cakes in their factory. Among Lewis and
Simon was Duncan, Sjin, and Sips. Duncan worked alongside Lewis and Simon,
and Sjin teamed up with Sips to create their own factory with the goal to
create infinite dirt...or to create...the purest dirt possible?? I don't--

Anyway.

*The Yogscast* made a stand in *Minecraft*'s popularity, spawning even more
YouTube channels dedicated to *Minecraft*. Popular examples included
*AntVenom*, *SkyDoesMinecraft* and his friends, *ChimneySwift11* who has all
since disappeared unfortunately, *TwoAwesomeGamers*, and many, many, MANY
others.

### Why Watch When You Can Play?

When the YouTube sensation was at it's peak, *Minecraft* had gained a massive
boost in sales and has been growing ever since. The players who watched the
videos wanted to play for themselves and went out to buy the game on the
old website, which you can actually still see using a website called *The
Wayback Machine*. The charm of the game, the community, and the fun that
you could have with your friends online with different maps and modes and
servers, the art that was made, everything had absolutely sold the game to
some people. The entire reason *Minecraft* gained any traction at all is
because it's a game where you could do anything you imagined. People
imagined, created, acted out and shared so much over the internet and made
so much with it that it became a world of beauty and passion. People wanted
to play because they wanted to do the same.

### Just One More Block...

*Minecraft* nowadays doesn't have the same charm to it as it did when we
first bought it. Maybe you had gotten it in 2009, maybe you just bought it
last week and are reading this article now, completely unaware of the
people who cultivated this game way back when. It doesn't have the same
charm since the first time you opened it up and played because, as *jschlatt*
had wonderfully expressed in his video, [*A Tribute to Minecraft*](https://www.youtube.com/watch?v=zNZ1rq5kW4M), part of the
experience of the game is learning how to play it. Once you know the game
inside and out, when you finally realize the game isn't the way you thought
of it originally, with it's whimsy and wonder and the "how can I do this
with what I have on hand" attitude, you fall into patterns. The same box
houses, the same aesthetics, the same routines. Then Minecraft doesn't
become an escape, it becomes another chore. Something that you *wanted*
escape from.

And once you realize that, you become free again.

When you realize that something is an issue, you don't just fall into the
same pattern that makes you feel like you have to do this, you want
something to change. Maybe for you it's a change in scenery. A change in
style. For you, it may be additions of different mechanics. The modding
scene is nowhere near as difficult as it was 5, 6 years ago. Go play some
tech mods, or magic mods. For you, it may just be that you're burnt out
from the game, and that's fine too. Minecraft is ageless. The only thing
that really matters is how old you're willing to get and how done with the
game you think you are. I wrote out a statement in a Discord server I'm in,
actually, when I was in the middle of writing the Persona game study.

April 25, 2019. I finished writing this at 7:21 PM.

"*Minecraft*, I feel, is like...in a real world perspective, I'd say it's
like a prebuilt house in the middle of nowhere. There's very little to it
to start off with, but what is there is charming. The house was built
sometime in the 1920s. It's quaint. Not much was added to it modernity-wise
except for updated hardware and air conditioning, but that's about it. It's
nice.

You can add to this house. That's what modding is. The house alone is
perfectly acceptable to live in, it has the appliances and everything there
for someone to live in on it's own. But you *can* add to it. The house
could stand to be a bit bigger, so basic framework is needed. The
equivalent would be Forge, Liteloader, or whatever other choices are out
there these days. As you add to the framework you realize there could be a
lot more here. People online have posted about how they've built their
houses. They've posted about what they've added, how they've added to it.
It's a glorious thing, making something else yourself. Living in that
world. You try making your own additions too - some have tried and
succeeded, some have failed. There are houses out there that have a unique
structure to them, almost linearly guiding you through them.

But there's nothing wrong with that quaint little house you started off in.
Sometimes you want to go back to those times, and there's nothing wrong
with that. The house you left behind will still be there, waiting for you
to come back. But what ultimately matters is what houses you enjoy your
time in. When the next new house is available, you leave and move out of
that one house and go into the next one. But reminiscing isn't something
you need to do with *Minecraft*. All the previous versions are still there.
The houses you've lived in, the houses you know are there and you know
every facet of them. *Minecraft* doesn't erode or rot. *Minecraft* just keeps
getting newer, and people make more and more mods. *Minecraft* is a house
that never fades."

I still stand by this statement to a certain extent. *Minecraft* isn't just a
house. It's a neighborhood. It's a community. It's a place where you can go
and have fun and nobody will judge you for how you do it. You're in your
own little private bubble, building what you want, building with your
friends. *Minecraft* is a place for *you*. And sometimes, that's all people
really need.